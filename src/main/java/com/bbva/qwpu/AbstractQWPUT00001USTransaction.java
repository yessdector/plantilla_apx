package com.bbva.qwpu;

import java.util.Arrays;
import java.util.Map;

import com.bbva.elara.domain.transaction.ParameterTable;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractQWPUT00001USTransaction extends AbstractTransaction {

	public AbstractQWPUT00001USTransaction(){
	}
	/**
	 * Return value for input parameter PARAM_SIMPLE_1
	 */
	protected String getParamSimple1()
	{
		return (String)getParameter("PARAM_SIMPLE_1");
	}
	
	protected String getParamHijo1OfParamCompuesto1(int rowNumber)
	{
		ParameterTable param = (ParameterTable)this.getParamCompuesto1();
		Map<String,Object> row = param.get(rowNumber);
		return (String)row.get("PARAM_HIJO_1");
	}
	protected ParameterTable getParamCompuesto1()
	{
		return (ParameterTable)this.getParameter("PARAM_COMPUESTO_1");
	}
	
	/**
	 * Set value for output parameter PARAM_SIMPLE_1
	 */
	protected void setParamSimple1(final String field)
	{
		this.addParameter("PARAM_SIMPLE_1", field);
	}
	
	
	/**
	 * Set value for compound output parameter PARAM_COMPUESTO_1
	 */
	protected void setParamCompuesto1(final ParameterTable field)
	{
		this.addParameter("PARAM_COMPUESTO_1", field);
	}
	
	private ParameterTable getOutputParamCompuesto1()
	{
		return (ParameterTable)this.getParameter("PARAM_COMPUESTO_1");
	}
	
	/**
	 * Insert a new row in compound output parameter PARAM_COMPUESTO_1
	 */
	protected void setParamCompuesto1Row(final Map<String, Object> rowToInsert)
	{
		ParameterTable param = (ParameterTable)this.getParameter("PARAM_COMPUESTO_1");
		if( param == null )
		{		
			param = new ParameterTable(Arrays.asList(new String[] {"PARAM_HIJO_1"}));
		}
		param.add(rowToInsert);
		this.setParamCompuesto1(param);
	}
}
